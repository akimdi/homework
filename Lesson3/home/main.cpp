#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cctype>
#include <string.h>
#include <iomanip>

using namespace std;

const int N = 100;

//Прототипы функций
void Read();//оболочка для чтение ВСЕХ начальных данных
void ReadInf(string nameFile, char inf);//функция чтение начальных данных
void WriteIdent(string buffer);
int CheckIdent(string buffer);
int FindWord(string buffer);
int FindSeparation(char h = 0);
int FindOperation(char h = 0);
int CheckNumb(string a);
int CheckChar(string a);
void OutNumb(string str);
void error();
void otherInformation();
//конец

void itoa(int i, char t[], int i1);

struct data
{
    string main;
    string code;
    int priority;
};

vector <struct data> Word;
vector <struct data> Operation;
vector <struct data> Separation;
vector <struct data> Char;
vector <struct data> ID;
vector <struct data> Number;

fstream in("perl.txt", ios::in | ios::out);
fstream out("out №1.txt", ios::out | ios::trunc);

int main()
{
    Read();//чтение начальной информации
    //in.seekg(0, ios::end);
    //in << "   ";
    //in.seekg(0, ios::beg);

    string buf; //буфер для лексемы
    char ch, _ch[100]="\0"; //буфер для символа

    while( !in.eof() )
    {
        while ((ch = in.get()) != 10 && !in.eof() )
        {
            buf.clear();
            if (ch == '#')//считывание комментария
            {
                char buffer[200];
                in.getline(buffer, 199, 10);
                out << endl;
                continue;
            }
            else if (ch == ' ' || ch == '\t')
            {
                continue;
            }

            if (ch == '@' || ch == '$')
            {
                buf += ch;
                ch = in.get();

                if (isalpha(ch) || ch == '_')
                {
                    buf += ch;
                    while (isalnum(ch = in.get()) || ch == '_')
                    {
                        buf += ch;
                    }
                    in.seekg(-1, ios::cur);//можно сказать убираем последний считанный символ

                    if (ch == 10 || in.eof() || (!(FindOperation() && FindSeparation())))
                    {
                        WriteIdent(buf);//записываем идентификатор
                        continue;
                    }
                    else
                    {
                        error();
                    }
                }
                else
                {
                    error();
                }
            }
            else if (!FindOperation(ch))
            {
            }
            else if (!FindSeparation(ch))
            {
            }
            else if (ch == '\'' || ch =='\"')
            {
                char _tbuf = ch;
                ch = in.get();
                int i = 0;
                _ch[i++] += ch;
                while(_tbuf != (ch=in.get()))
                {
                    if(ch == -1 || ch == '\n')
                        error();
                    _ch[i++] += ch;
                }
                //проверка
                for(unsigned int i = 0; i < strlen(_ch); ++i)
                {
                    if (_ch[i] == '\n')
                        error();
                }
                buf = _ch;
                for (int i = 0; i < 99;++i)
                    _ch[i] = 0;
                if(CheckChar(buf))
                {
                    string a("C");
                    char t[3];
                    itoa((int)Char.size(), t, 10);
                    a += t;
                    struct data temp = {buf, a, -1};
                    Char.push_back(temp);
                    out << temp.code << ' ';//добаление
                }
            }
            else if(isalpha(ch))
            {
                buf += ch;
                while (isalnum(ch = in.get()) || ch == '_')
                {
                    buf += ch;
                }
                in.seekg(-1, ios::cur);//можно сказать убираем последний считанный символ
                if (ch == 10 || in.eof() || (!(FindOperation() && FindSeparation())))
                {
                    if (FindWord(buf))//записываем слово
                        WriteIdent(buf);//записываем идентификатор( метку)
                    continue;
                }
                else
                {
                    error();
                }
            }
            else if (ch == '.')//считывание численной константы №1
            {
                buf += ch;
                ch = in.get();
                buf += ch;
                if (isdigit(ch))
                {
                    while (isdigit(ch = in.get()))
                    {
                        buf += ch;
                    }
                    in.seekg(-1, ios::cur);
                    if (ch == 'e' || ch == 'E')
                    {
                        ch = in.get();
                        buf += ch;
                        ch = in.get();
                        if (ch == '+' || ch == '-')
                        {
                            buf += ch;
                            while (isdigit(ch = in.get()))
                            {
                                buf += ch;
                            }
                            in.seekg(-1, ios::cur);
                            if (!(FindOperation() && FindSeparation()))
                            {
                                OutNumb(buf);//записываем число
                            }
                            else
                                error();
                            continue;
                        }
                        else if (isdigit(ch))
                        {
                            buf += ch;
                            while (isdigit(ch = in.get()))
                            {
                                buf += ch;
                            }
                            in.seekg(-1, ios::cur);
                            if (!(FindOperation() && FindSeparation()))
                            {
                                OutNumb(buf);//записываем число
                            }
                            else
                                error();
                        }
                        else
                        {
                            error();
                        }
                    }
                    else if (!(FindOperation() && FindSeparation()))
                    {
                        OutNumb(buf);//записываем число
                    }
                    else
                        error();
                }
                else
                {
                    error();
                }
                continue;
            }
            else if (isdigit(ch))//считывание численной константы №2
            {
                buf += ch;
                while (isdigit(ch = in.get()))
                {
                    buf += ch;
                }
                in.seekg(-1, ios::cur);
                if (ch == '.')
                {
                    ch = in.get();
                    buf += ch;
                    ch = in.get();
                    in.seekg(-1, ios::cur);
                    if (isdigit(ch))
                    {
                        ch = in.get();
                        buf += ch;
                        while (isdigit(ch = in.get()))
                        {
                            buf += ch;
                        }
                        in.seekg(-1, ios::cur);
                        if (ch == 'e' || ch == 'E')
                        {
                            ch = in.get();
                            buf += ch;
                            ch = in.get();
                            if (ch == '+' || ch == '-')
                            {
                                buf += ch;
                                while (isdigit(ch = in.get()))
                                {
                                    buf += ch;
                                }
                                in.seekg(-1, ios::cur);
                                if (!(FindOperation() && FindSeparation()))
                                {
                                    OutNumb(buf);//записываем число
                                }
                                else
                                    error();
                                continue;
                            }
                            else if (isdigit(ch))
                            {
                                buf += ch;
                                while (isdigit(ch = in.get()))
                                {
                                    buf += ch;
                                }
                                in.seekg(-1, ios::cur);
                                if (!(FindOperation() && FindSeparation()))
                                {
                                    OutNumb(buf);//записываем число
                                }
                                else
                                    error();
                            }
                            else
                            {
                                error();
                            }
                        }
                        else if (!(FindOperation() && FindSeparation()))
                        {
                            OutNumb(buf);//записываем число
                        }
                        else
                            error();
                    }
                    else if (!(FindSeparation()&&FindOperation()))
                    {
                        OutNumb(buf);//записываем число
                    }
                    else
                    {
                        error();
                    }
                }
                else if (!(FindSeparation() && FindOperation()))
                {
                    OutNumb(buf);//записываем число
                }
                else
                    error();
            }
            else
            {
                error();
            }

        }
        out << endl;
    }

    in.close();
    out.close();

    otherInformation();

    return 0;
}

void itoa(int i, char t[], int i1) {

}

void WriteIdent(string buffer)
{
    if (!CheckIdent(buffer))
    {
        string a("I");
        char t[3];
        itoa((int)ID.size(), t, 10);
        a += t;

        struct data temp;
        temp.main = buffer;
        temp.code = a;

        ID.push_back(temp);
        out << temp.code << ' ';
    }
}

int CheckIdent(string buffer)//возвращаем 1, если найден такой уже идентификатор, и 0 в противном случае
{
    for(vector<struct data>::iterator Item = ID.begin(); Item < ID.end(); Item++)
    {
        if(!buffer.compare(Item->main))
        {
            out << Item->code << ' ';
            return 1;
        }
    }
    return 0;
}

int FindOperation(char h)//возвращает 0, если найдена операция, и 1 в противном случае
{//выведем найденую операцию, если входной параметр был не по умолчанию
    char ch;
    string buf1, buf2;
    if (h == 0)
    {
        ch = in.get();
        buf1 = ch;
        buf2 = ch;
    }
    else
    {
        buf1 = h;
        buf2 = h;
    }
    ch = in.get();
    buf2 += ch;// для проверки 2х знаковых операций
    if (h == 0)
    if (ch == '\n')
        in.seekg(-3, ios::cur);
    else
        in.seekg(-2, ios::cur);
    else
        in.seekg(-1, ios::cur);// возвращаем то, место откуда мы считали символ
    if (in.fail())
    {
        out << "file error"<<endl;
        error();
    }
    for(vector<struct data>::iterator Item = Operation.begin(); Item < Operation.end(); Item++)
    {
        if (!buf2.compare(Item->main))
        {
            if (h != 0)
            {
                out << Item->code << ' ';
                in.seekg(1, ios::cur);
            }
            return 0;
        }
    }
    for(vector<struct data>::iterator Item = Operation.begin(); Item < Operation.end(); Item++)
    {
        if (!buf1.compare(Item->main))
        {
            if (h != 0)
                out << Item->code << ' ';
            return 0;
        }
    }
    return 1;
}

int FindSeparation(char h)//возвращает 0, если найден разделитель, и 1 в противном случае
{
    char ch;
    string buf;
    if (h ==0)
    {
        ch = in.get();
        in.seekg(-1, ios::cur);// возвращаем то, место откуда мы считали символ
        buf = ch;
    }
    else
    {
        buf = h;
    }
    for(vector<struct data>::iterator Item = Separation.begin(); Item < Separation.end(); Item++)
    {
        if (!buf.compare(Item->main))
        {
            if ( h != 0)
                out << Item->code << " ";
            return 0;
        }
    }
    if (buf == " " || buf == "\n")
        return 0;
    return 1;
}

void error()
{
    otherInformation();
    cout << "Error!!!" << endl;
    system("PAUSE");
    exit(0);
}

void otherInformation()
{
    //------------------------------------------------------------------------------------------------------------------
    fstream fID("ID.txt", ios::out | ios::trunc);
    fstream fNumber("Number.txt", ios::out | ios::trunc);
    fstream fChar("Char.txt", ios::out | ios::trunc);

    for(vector<struct data>::iterator Item = ID.begin(); Item < ID.end(); Item++)
        fID << Item->main << ' '<< Item->code << endl;

    for (vector<struct data>::iterator Item = Number.begin (); Item < Number.end (); Item++)
        fNumber << Item->main << ' '<< Item->code << endl;

    for(vector<struct data>::iterator Item = Char.begin(); Item < Char.end(); Item++)
        fChar << Item->main << '\n'<< Item->code << endl;
}

int CheckChar(string a)//затычка, возвращает 0, если константа символьная найдена(и сразу выводит номер его в файл!), и 1 в противоположном случае!
{
    for(vector<struct data>::iterator Item = Char.begin(); Item < Char.end(); Item++)
    {
        if (!a.compare(Item->main))
        {
            out << Item->code << ' ';
            return 0;
        }
    }
    return 1;
}

int FindWord(string buffer)//0, если слово найдено и 1 в противном случае
{
    for(vector<struct data>::iterator Item = Word.begin(); Item < Word.end(); Item++)//поиск служебного слова
    {
        if (!buffer.compare(Item->main))
        {
            out << Item->code << ' ';
            buffer.clear();
            return 0;
        }
    }
    return 1;
}

void OutNumb(string str)
{
    if (CheckNumb(str))//если числа нет в списке, то оно добавляется!
    {
        string a("N");
        char t[3];
        itoa((int)Number.size(), t, 10);
        a += t;
        struct data temp = {str, a};
        Number.push_back(temp);
        out << temp.code << ' ';
    }
}

int CheckNumb(string a)
{
    for (vector<struct data>::iterator Item = Number.begin (); Item < Number.end (); Item++)
    {
        if(!a.compare(Item->main))
        {
            out << Item->code << ' ';
            return 0;
        }
    }
    return 1;
}

void ReadInf(string nameFile, char inf)
{
    fstream file(nameFile, ios::in);// открываем для чтениея
    char _buf[N] = "\0";
    char _code[N] = "\0";
    int priority = -1;

    while(!file.eof())
    {
        if (inf == 'C' || inf == 'N' || inf == 'I')//разбиваем на группы с приоритет и без приоритета!
        {//группа без приоритета
            if (inf == 'C')//считывание символьных констант имеет отдельное место
            {
                //считывание символьной констант!
                file.getline(_buf, sizeof(_buf), '\n');
                file.getline(_code, sizeof(_code), '\n');
            }
            else
            {
                file.getline(_buf, sizeof(_buf), ' ');
                file.getline(_code, sizeof(_code), ' ');
            }
        }
        else
        {
            file.getline(_buf, sizeof(_buf), ' ');
            file.getline(_code, sizeof(_code), ' ');
            file >> priority;
            file.get();
        }

        struct data tmp = {_buf, _code, priority};

        if (inf == 'C')
            Char.push_back(tmp);
        else if (inf == 'N')
            Number.push_back(tmp);
        else if (inf == 'I')
            ID.push_back(tmp);
        else if (inf == 'S')
            Separation.push_back(tmp);
        else if (inf == 'O')
            Operation.push_back(tmp);
        else if (inf == 'W')
            Word.push_back(tmp);

        memset(_buf, 0, sizeof(_buf));//очистка буфера
        memset(_code, 0, sizeof(_code));//очистка буфера
        priority = -1;
    }
    //удаление последнего элемента, т.к. он получился у нас пустым
    if (inf == 'C')
        Char.pop_back();
    else if (inf == 'N')
        Number.pop_back();
    else if (inf == 'I')
        ID.pop_back();
    else if (inf == 'S')
        Separation.pop_back();
    else if (inf == 'O')
        Operation.pop_back();
    else if (inf == 'W')
        Word.pop_back();

    file.close();
}

void Read()
{
    //--------------------------------------------чтение входных данных----------------------------------------------------
    ReadInf((string)"Word.txt", 'W');
    ReadInf((string)"Separation.txt", 'S');
    ReadInf((string)"Operation.txt", 'O');
    //---------------------------------------------------------------------------------------------------------------------
}